﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaTestApp.dal
{
    public class Dal
    {
        private static List<string> authorsDefault = new List<string> { "Толстой", "Достоевский", "Булгаков", "Хэмингуэй", "Пушкин", "Пелевин", "Фаулер" };
        private static List<string> books = new List<string> { "Война и мир", "Преступление и наказание", "Записки молодого врача", "Старик и море", "Онегин", "Чапаев и пустота", "Объектно орентированный анализ и дизайн" };
        private static List<string> countries = new List<string> { "Россия", "США", "Сингапур", "Китай", "Австралия", "ОАЭ", "Мексика" };
        private static List<string> langs = new List<string> { "Русский", "Английский", "Китайский", "Арабский" };

        /// <summary>        
        /// Добавляем тестовые данные в таблицы 
        /// </summary>
        public static void AddTestData()
        {
            if (false) {
                using (var dc = new bookrepositoryEntities1())
                {
                    var rand = new Random();
                    //добавляем писателей
                    for (var i = 0; i < 1000; i++)
                    {
                        var date = DateTime.Now
                            .AddYears(rand.Next(300) * -1)
                            .AddMonths(rand.Next(12) * -1)
                            .AddDays(rand.Next(30) * -1);

                        dc.Authors.Add(new Authors
                        {
                            Name = string.Format("{0}_{1}", authorsDefault[i % authorsDefault.Count], i),
                            BirthDate = date,
                            Country = countries[rand.Next(6)]

                        });
                    }

                    dc.SaveChanges();

                }
            }
           

            //Добавляем книги 
            var tasks = Enumerable.Range(0, 99).Select(offset => Task.Run(() =>
            {
                using (var dc = new bookrepositoryEntities1())
                {
                    var rand = new Random();
                    var rowCount = 1000;

                    for (var i = offset * rowCount; i < (offset + 1) * rowCount; i++)
                    {
                        var authId = rand.Next(1000);
                        var author = dc.Authors.FirstOrDefault(a => a.Id == (authId == 0 ? 1 : authId));
                        var title = books[i % books.Count];
                        var date = author.BirthDate.AddYears(20 + rand.Next(50));
                        dc.Books.Add(new Books
                        {
                            AuthorId = author.Id,
                            Title = string.Format("{0}_{1}", title, i),
                            IssueDate = date,
                            ISBNcode = string.Format("{0}-{1}-{2}", rand.Next(6), rand.Next(300), i),
                            ShortDesc = string.Format("Author - {0}, Title - {1}, Year - {2}, Rating - {3}", author.Name, books, title, date.Year),
                            Lang = langs[i % langs.Count]
                        });
                    }

                    dc.SaveChanges();
                }
            })).ToArray();

            Task.WaitAll(tasks);

        }


    }
}
