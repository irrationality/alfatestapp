﻿using System;
using System.Web.Mvc;
using AlfaTestAppDal;
using AlfaTestApp.Models;

namespace AlfaTestApp.Controllers
{
    public class HomeController : Controller
    {
        private IDal _dal;

        public HomeController(IDal dal)
        {
            _dal = dal;
        }

        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Cтраница отчета
        /// </summary>        
        public ActionResult ReportPage() {
            return View();
        }

        /// <summary>
        /// Запрос данных отчета
        /// </summary>        
        public JsonResult GetReportData() {
            try
            {
                var data = _dal.GetReportData();
                return Json(new { data, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Генерация тестовых данных
        /// </summary>        
        public JsonResult GenerateTestData()
        {
            try
            {
                _dal.AddTestData();
                return Json(new { status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}