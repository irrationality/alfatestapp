﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlfaTestAppDal;
using AlfaTestApp.Models;

namespace AlfaTestApp.Controllers
{
    public class AuthorController : Controller
    {
        private IDal _dal;

        public AuthorController(IDal dal)
        {
            _dal = dal;
        }

        public ActionResult Index()
        {
            return View("Authors");
        }

        /// <summary>
        ///  страница каталога книг конкретного автора
        /// </summary>        
        public ActionResult AuthorsBooksPage(int authId)
        {

            ViewBag.AuthId = authId;
            return View();
        }

        /// <summary>
        /// Получение данных ajaxом
        /// </summary>        
        public JsonResult GetAuthorsBooksData(int authId)
        {
            try
            {
                var data = _dal.GetAuthorsBooksPageData(authId);
                return Json(new { data, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error }, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Получение данных ajaxом
        /// </summary>        
        public JsonResult GetAuthors(int page, int count)
        {
            try
            {
                var data = _dal.GetAuthorsPageData(page, count);
                return Json(new { data, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Обработчик ajax-запроса на добавление автора
        /// </summary>       
        public JsonResult AddAuthor(string name, string country, string bDate)
        {
            try
            {
                DateTime date;

                if (!DateTime.TryParse(bDate, out date)) throw new Exception("Дата рождения указана в неправильном формате");
                var author = new Authors
                {
                    Name = name,
                    Country = country,
                    BirthDate = date
                };

                var addedAuthor = _dal.AddAuthor(author);
                return Json(new { author = addedAuthor, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
