﻿using System;
using System.Web.Mvc;
using AlfaTestApp.Models;
using AlfaTestAppDal;

namespace AlfaTestApp.Controllers
{
    public class BookController : Controller
    {
        private IDal _dal;

        public BookController(IDal dal)
        {
            _dal = dal;
        }

        // GET: Book
        public ActionResult Index()
        {
            return View("Books");
        }

        /// <summary>
        ///Постраничная подгрузка данных о книгах 
        /// </summary>      
        public JsonResult GetBooks(int page, int count, string searchTerm)
        {
            try
            {
                var data = _dal.GetBooksPageData(page, count, searchTerm);
                return Json(new { data, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error },JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Обработчик ajax-запроса на добавление автора
        /// </summary>       
        public JsonResult AddBook(BookModel bookModel)
        {
            try
            {
               
                var book = new Books
                {
                   Title = bookModel.title,
                   ShortDesc = bookModel.shortDesc,
                   IssueDate = new DateTime(bookModel.year, 1, 1),
                   AuthorId = bookModel.authorId,
                   ISBNcode = bookModel.ISBNcode,
                   Lang = bookModel.lang                    
                };

                var addedBook = _dal.AddBook(book);
                return Json(new { book = addedBook, status = AjaxCodes.Success.ToString() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { errMessage = ex.Message, status = AjaxCodes.Error.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
