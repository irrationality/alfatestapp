/****** Script for SelectTopNRows command from SSMS  ******/
use bookrepository
go

create procedure getReportData
as
begin

DECLARE @total int
select @total = count(1) FROM [bookrepository].[dbo].[Books]

DECLARE @BookCounts TABLE
(
  AuthorId int, 
  BookCount int,
  Share float,
  Total int
) 

INSERT INTO @BookCounts (AuthorId, BookCount, Share, Total )
 
SELECT AuthorId, count(1) as BookCount , Cast(count(*) as float)/cast(@total as float), @total
  FROM [bookrepository].[dbo].[Books] 
  group by AuthorId  


  select a.Name, b.BookCount, b.Share from
  @BookCounts as b 
  inner join 
  [bookrepository].[dbo].[Authors] as a
  on a.Id = b.AuthorId

  end


  GRANT EXECUTE ON dbo.getReportData
    TO AlfaTestApp;
GO




  
