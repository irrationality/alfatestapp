USE master;
GO
IF DB_ID (N'bookrepository') IS NOT NULL
DROP DATABASE bookrepository;
GO


CREATE DATABASE bookrepository;
GO
-- Verify the database files and sizes
SELECT name, size, size*1.0/128 AS [Size in MBs] 
FROM sys.master_files
WHERE name = N'bookrepository';
GO



use bookrepository

if OBJECT_ID('dbo.Authors') is not null 
drop table dbo.Authors

create table dbo.Authors(
	
	Id int not null identity(1,1), 
	Name varchar(256) not null,
	BirthDate date not null,
	Country varchar(128) not null, 
	constraint pk_Authors primary key(Id)


)


if OBJECT_ID('dbo.Books') is not null 
drop table dbo.Books

create table dbo.Books(
	
	Id int not null identity(1,1), 
	Title varchar(256)  null,
	IssueDate date not null,
	AuthorId int not null, 
	ShortDesc varchar(512) ,
	ISBNcode varchar(30) not null,
	Lang varchar(128)
	constraint pk_Books primary key(Id)
	constraint fk_Authors foreign key(AuthorId)
	references dbo.Authors(Id)
)

CREATE LOGIN AlfaTestApp
    WITH PASSWORD = 'qwertyuio';
GO


CREATE USER AlfaTestApp FOR LOGIN AlfaTestApp;
GO

use bookrepository

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE  ON  dbo.Authors TO AlfaTestApp;
GO

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE  ON  dbo.Books TO AlfaTestApp;
GO