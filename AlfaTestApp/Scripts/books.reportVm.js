﻿var booksRepository = booksRepository || {};

(function (model) {


    model.reportRows = ko.observableArray();

    //Подгружаем ajaxom данны
    $.ajax({
        url: booksRepository.urls.loadReportData,
        type: 'GET',
        success: function (resp) {
            if (resp.status == success) {
                var data = resp.data;                
                model.reportRows(data)
            } else {
                alert(resp.errMEssage);
            }
        }
    });
})(booksRepository)