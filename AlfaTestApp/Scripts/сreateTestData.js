﻿var booksRepository = booksRepository || {};

(function (model) {

    model.createTestDataBtnText = ko.observable('Cгенерировать тестовые данные ');
    model.createTestData = function () {
        model.createTestDataBtnText('Дождитесь окончания обработки запроса');
        $.ajax({
            url: booksRepository.urls.generateTestData,
            type: 'GET',
            success: function (resp) {
                if (resp.status == success) {

                    alert('Данные сгенерированы');
                    model.createTestDataBtnText('Cгенерировать тестовые данные ');
                } else {
                    alert(resp.errMEssage);
                }
            }
        });
    }    
    
})(booksRepository)