﻿var booksRepository = booksRepository || {};

(function (model) {    

    model.authors = ko.observableArray();
    model.currentPage = ko.observable(1);
    model.countPerPage = 100;
    
    model.pagesCount = ko.observable(0);

    model.name = ko.observable();
    model.country = ko.observable(),
    model.bDate = ko.observable()

    //Добавить автора
    model.addAuthor = function () {
        $.ajax({
            url: booksRepository.urls.addAuthor,
            type: 'POST',
            data: {
                name: model.name(),
                country: model.country(),
                bDate: model.bDate()
            },
            success: function (resp) {
                if (resp.status == success) {
                    $('#myModal').modal('hide');
                    model.authors.unshift(resp.author);

                } else {
                    alert(resp.errMessage);
                }
            }
        });
    }

    //Подгружаем ajaxom данные об авторах
    model.loadData = function (currentPage) {
        $.ajax({
            url: booksRepository.urls.loadAuthors,
            type: 'GET',
            data: {
                page: currentPage,
                count: model.countPerPage                
            },
            success: function (resp) {
                if (resp.status == success) {
                    var data = resp.data;
                    model.pagesCount(data.pagesCount);
                    model.authors(data.authors.map(function (author) { return author; }))
                } else {
                    alert(resp.errMessage);
                }
            }
        });
    }

    model.nextPage = function () {
        model.currentPage(model.currentPage() + 1);
    }

    model.previousPage = function () {
        var currentPage = model.currentPage();
        if (currentPage == 1) return;
        model.currentPage(currentPage - 1);
    }

    // Апдейтер данных страницы
    var updater = ko.computed(function () {
        var page = model.currentPage();
        model.loadData(page);
    });



})(booksRepository)