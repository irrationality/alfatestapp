﻿var booksRepository = booksRepository|| {};

(function (model) {

    var bookVM = function () {

    }

    model.books = ko.observableArray();
    model.currentPage = ko.observable(1);
    model.countPerPage = 100;
    model.searchTerm = ko.observable('');
    model.pagesCount = ko.observable(0);
    //Подгружаем ajaxom данные о книгах 
    model.loadData = function (currentPage,searchTerm) {
        $.ajax({
            url: booksRepository.urls.loadBooks,
            type: 'GET',
            data: {
                page: currentPage,
                count: model.countPerPage,
                searchTerm: searchTerm
            },
            success: function (resp) {               
                if (resp.status == success) {
                    var data = resp.data;
                    model.pagesCount(data.pagesCount);
                    model.books(data.books.map(function (book) { return book; }))
                } else {
                    alert(data.errMEssage);
                }
            }
        });
    }

    model.nextPage = function () {

        model.currentPage(model.currentPage() + 1);
    }

    model.previousPage = function () {
        var currentPage = model.currentPage();
        if (currentPage == 1) return;
        model.currentPage(currentPage - 1);
    }
    
    var updater = ko.computed(function () {
        var page = model.currentPage() - 1;
        var searchTerm = model.searchTerm();

        model.loadData(page,searchTerm);
    });
  
    
  
})(booksRepository)