﻿var booksRepository = booksRepository || {};

(function (model) {

    var authorVM = function () {

    }

    model.books = ko.observableArray();
    model.author = ko.observable();
    model.id = ko.observable();

    
    model.title = ko.observable();
    model.shortDesc = ko.observable();
    model.ISBNcode = ko.observable();
    model.year = ko.observable();
    model.lang = ko.observable();
    

    //Добавить книгу
    model.addBook = function () {
        $.ajax({
            url: booksRepository.urls.addBook,
            type: 'POST',
            data: {
                authorId: model.author().id,
                title: model.title(),
                shortDesc: model.shortDesc(),
                ISBNcode: model.ISBNcode(),
                year: model.year(),
                lang: model.lang(),
            },
            success: function (resp) {
                if (resp.status == success) {
                    $('#myModal').modal('hide');
                    model.books.unshift(resp.book);

                } else {
                    alert(resp.errMessage);
                }
            }
        });
    }
    
    //Подгружаем ajaxom данные 
    model.loadData = function (currentPage) {
        $.ajax({
            url: booksRepository.urls.loadAuthorsBooks,
            type: 'GET',
            data: {
                authId: booksRepository.authId
            },
            success: function (resp) {
                if (resp.status == success) {
                    var data = resp.data;
                    model.author(data.author);
                    model.books(data.books.map(function (book) { return book; }))
                } else {
                    alert(data.errMEssage);
                }
            }
        });
    }

    model.loadData();
})(booksRepository)