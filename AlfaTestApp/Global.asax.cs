﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AlfaTestAppDal;
using NLog;

//elelisrael1990

namespace AlfaTestApp
{
    
    public class MvcApplication : HttpApplication
    {
        private static Logger _log = LogManager.GetCurrentClassLogger();
        protected void Application_Start()
        {
            _log.Debug("first record");
            //Dal.AddTestData();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //инициализация di контейнера 
            DiConfig.RegisterDi();   
        }
    }
}
