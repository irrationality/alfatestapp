﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaTestAppDal
{
    public interface IDal
    {
        /// <summary>
        /// Добавление тестовых данных в базу 
        /// </summary>
        void AddTestData();

        /// <summary>
        /// Получение данных для страницы авторов
        /// </summary>       
        AuthorsPageDataModel GetAuthorsPageData(int page, int count);

        /// <summary>
        /// Получение данных страницы каталого автор книги
        /// </summary>       
        AuthorsBooksPageModel GetAuthorsBooksPageData(int authId);

        /// <summary>
        /// Получение данных для страницы книг
        /// </summary>      
        BookPageDataModel GetBooksPageData(int page, int count, string searchTerm = null);

        /// <summary>
        /// Добавление автора         
        /// </summary>        
        AuthorModel AddAuthor(Authors author);

        /// <summary>
        /// Добавление книги
        /// </summary>        
        BookModel AddBook(Books book);

        /// <summary>
        ///  Получить данные для страницы отчета
        /// </summary>        
        List<ReportModel> GetReportData();
    }
}
