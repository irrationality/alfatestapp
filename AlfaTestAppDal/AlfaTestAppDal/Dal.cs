﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace AlfaTestAppDal
{
    /// <summary>
    /// Cлой доступа к данным
    /// </summary>
    public class Dal : IDal
    {
        /// <summary>
        /// Данные для генерации тестовых сргок  в БД
        /// </summary>
        private static List<string> authorsDefault = new List<string> { "Толстой", "Достоевский", "Булгаков", "Хэмингуэй", "Пушкин", "Пелевин", "Фаулер" };
        private static List<string> books = new List<string> { "Война и мир", "Преступление и наказание", "Записки молодого врача", "Старик и море", "Онегин", "Чапаев и пустота", "Объектно орентированный анализ и дизайн" };
        private static List<string> countries = new List<string> { "Россия", "США", "Сингапур", "Китай", "Австралия", "ОАЭ", "Мексика" };
        private static List<string> langs = new List<string> { "Русский", "Английский", "Китайский", "Арабский" };


        private static Logger _log = LogManager.GetCurrentClassLogger();
        /// <summary>        
        /// Добавляем тестовые данные в таблицы 
        /// </summary>
        public void AddTestData()
        {
            try
            {
                using (var dc = new booksEntities())
                {
                    var rand = new Random();
                    //добавляем писателей
                    for (var i = 0; i < 1000; i++)
                    {
                        var date = DateTime.Now
                            .AddYears(rand.Next(300) * -1)
                            .AddMonths(rand.Next(12) * -1)
                            .AddDays(rand.Next(30) * -1);

                        dc.Authors.Add(new Authors
                        {
                            Name = string.Format("{0}_{1}", authorsDefault[i % authorsDefault.Count], i),
                            BirthDate = date,
                            Country = countries[rand.Next(6)]

                        });
                    }

                    dc.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                _log.Error("AddTestData. Adding authors exception - {0}", ex.Message);
            }

            //Добавляем книги, для скорости запускаем параллельно 100 тасков добавления 
            var tasks = Enumerable.Range(0, 99).Select(offset => 
            Task.Run(() =>
            {
                try
                {
                    using (var dc = new booksEntities())
                    {
                        var rand = new Random();                // рандомайзер
                        var rowCount = 1000;                    // кол-во строк добавляемых в одном таске 
                        var authorsCount = dc.Authors.Count();  // кол-во записей в таблице авторов

                        for (var i = offset * rowCount; i < (offset + 1) * rowCount; i++)
                        {
                            var skipTo = rand.Next(0, authorsCount);
                            var author = dc.Authors.OrderBy(a => a.Id).Skip(skipTo).Take(1).First();
                            var title = books[i % books.Count];
                            var date = author.BirthDate.AddYears(20 + rand.Next(50));
                            dc.Books.Add(new Books
                            {
                                AuthorId = author.Id,
                                Title = string.Format("{0}_{1}", title, i),
                                IssueDate = date,
                                ISBNcode = string.Format("{0}-{1}-{2}", rand.Next(6), rand.Next(300), i),
                                ShortDesc = string.Format("Author - {0}, Title - {1}, Year - {2}, Rating - {3}", author.Name, books, title, date.Year),
                                Lang = langs[i % langs.Count]
                            });
                        }

                        dc.SaveChanges();

                    }
                }
                catch (Exception ex)
                {
                    _log.Error("AddTestData. Adding books exception - {0}", ex.Message);
                }
            })).ToArray();


            Task.WaitAll(tasks);

        }

        /// <summary>
        /// Получение данных страницы авторов
        /// </summary>       
        public AuthorsPageDataModel GetAuthorsPageData(int page, int count)
        {
            try
            {
                using (var context = new booksEntities())
                {
                    var pageCount = (int)Math.Round((decimal)(context.Authors.Count()) / 100);

                    var authors = context.Authors.
                        OrderBy(b => b.Id)
                        .Skip(count * page).
                        Take(count).ToList();

                    return new AuthorsPageDataModel
                    {
                        pageCount = pageCount,
                        authors = authors.Select(a => new AuthorModel
                        {
                            birthDate = a.BirthDate.ToString("dd.MM.yyyy"),
                            country = a.Country,
                            id = a.Id,
                            name = a.Name,

                        }).ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                _log.Error(" Get authors page exception {0}", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Получение данных страницы каталого автор книги
        /// </summary>       
        public AuthorsBooksPageModel GetAuthorsBooksPageData(int authId)
        {
            try
            {
                using (var context = new booksEntities())
                {
                    var author = context.Authors.FirstOrDefault(a => a.Id == authId);

                    return new AuthorsBooksPageModel
                    {
                        author = new AuthorModel
                        {
                            birthDate = author.BirthDate.ToString("dd.MM.yyyy"),
                            name = author.Name,
                            country = author.Country,
                            id = author.Id
                        },
                        books = author.Books.Select(b => new BookModel
                        {
                            title = b.Title,
                            ISBNcode = b.ISBNcode,
                            year = b.IssueDate.Year,
                            shortDesc = b.ShortDesc
                        }).ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                _log.Error(" Get authors page exception {0}", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        ///  Получение данных страницы  книг
        /// </summary>        
        public BookPageDataModel GetBooksPageData(int page, int count, string searchTerm = null)
        {
            try
            {
                // пришел поисковый запрос - выполняем поиск
                if (!string.IsNullOrEmpty(searchTerm))
                {
                    return Search(page, count, searchTerm);
                }

                // Основная логика отдачи данных страницы книги
                using (var context = new booksEntities())
                {
                    var pageCount = (int)Math.Round((decimal)(context.Books.Count()) / 100);
                    var books = context.Books.
                        OrderBy(b => b.Id)
                        .Skip(count * page).
                        Take(count).ToList();

                    return new BookPageDataModel
                    {
                        books = books.Select(b => new BookModel
                        {
                            id = b.Id,
                            authorId = b.AuthorId,
                            title = b.Title,
                            shortDesc = b.ShortDesc,
                            ISBNcode = b.ISBNcode,
                            year = b.IssueDate.Year,
                            lang = b.Lang,
                            authorName = b.Authors.Name
                        }).ToList(),
                        pageCount = pageCount
                    };
                }
            }
            catch (Exception ex)
            {
                _log.Error(" Get books page exception {0}", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Поиск по вхождению строки запроса в название, имя автора, isbn, 
        /// </summary>    
        /// to do - возможно стоит добавить кэширование результатов для повышения производительности    
        private static BookPageDataModel Search(int page, int count, string searchTerm)
        {
            using (var context = new booksEntities())
            {
                var searchTermLow = searchTerm.ToLower();
                var searchRes = context.Books.
                    Where(b => b.Title.ToLower().Contains(searchTermLow) ||
                    b.ISBNcode.ToLower().Contains(searchTermLow) ||
                    b.Authors.Name.ToLower().Contains(searchTermLow));



                var pageCount = searchRes.Count();

                var books = searchRes.
                     OrderBy(b => b.Id).
                     Skip(count * page).
                     Take(count).ToList();

                return new BookPageDataModel
                {
                    books = books.
                    Select(b => new BookModel
                    {
                        id = b.Id,
                        authorId = b.AuthorId,
                        title = b.Title,
                        shortDesc = b.ShortDesc,
                        ISBNcode = b.ISBNcode,
                        year = b.IssueDate.Year,
                        lang = b.Lang,
                        authorName = b.Authors.Name
                    }).ToList(),
                    pageCount = pageCount
                };
            }

        }

        /// <summary>
        /// Добавляем автора
        /// </summary>
        public AuthorModel AddAuthor(Authors author)
        {
            try
            {
                using (var context = new booksEntities())
                {
                    context.Authors.Add(author);
                    context.SaveChanges();

                    return new AuthorModel
                    {
                        id = author.Id,
                        name = author.Name,
                        country = author.Country,
                        birthDate = author.BirthDate.ToString("dd.MM.yyyy")
                    };
                }

            }
            catch (Exception ex)
            {
                _log.Error("Add new author exception {0}", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Добавляем книгу 
        /// </summary>       
        public BookModel AddBook(Books book)
        {
            try
            {
                using (var context = new booksEntities())
                {
                    context.Books.Add(book);
                    context.SaveChanges();
                }

                return new BookModel
                {
                    id = book.Id,
                    title = book.Title,
                    shortDesc = book.ShortDesc,
                    ISBNcode = book.ISBNcode,
                    year = book.IssueDate.Year,
                    lang = book.Lang

                };
            }
            catch (Exception ex)
            {
                _log.Error("Add new book exception {0}", ex.Message);
                throw ex;
            }
        }

        /// <summary>
        ///  Получить данные для страницы отчета
        /// </summary>        
        public List<ReportModel> GetReportData() {
            try
            {
                using (var context = new booksEntities())
                {
                    return context.getReportData().Select(r => new ReportModel {
                         name = r.Name,
                         share = r.Share ?? 0,
                         bookCount = r.BookCount ?? 0
                    }).ToList();                   
                }
            }
            catch (Exception ex)
            {
                _log.Error("Get report data exception {0}", ex.Message);
                throw ex;
            }
        }
    }
}
