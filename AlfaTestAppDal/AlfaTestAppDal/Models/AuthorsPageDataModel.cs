﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaTestAppDal
{
    /// <summary>
    /// Модель для страницы авторов
    /// </summary>
    public class AuthorsPageDataModel
    {
        public List<AuthorModel> authors { get; set; }
        public int pageCount { get; set; }
    }

    public class AuthorModel {

        public string birthDate { get; set; }
        public string country { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }
}
