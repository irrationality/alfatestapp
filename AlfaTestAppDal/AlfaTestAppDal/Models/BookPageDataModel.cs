﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaTestAppDal
{
    /// <summary>
    /// Модель данных для страницы книг
    /// </summary>
    public class BookPageDataModel
    {
        public List<BookModel> books { get; set; }
        public int pageCount { get; set; }
    }

    public class BookModel
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public string title { get; set; }
        public string shortDesc { get; set; }
        public string ISBNcode { get; set; }
        public int year { get; set; }
        public string lang { get; set; }
        public string authorName { get; set; }
    }
}
