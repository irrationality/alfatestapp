﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaTestAppDal
{
    /// <summary>
    /// Модель страницы каталога автор книги 
    /// </summary>
    public class AuthorsBooksPageModel
    {
        public List<BookModel> books { get; set; }
        public AuthorModel author { get; set; }
    }
}
