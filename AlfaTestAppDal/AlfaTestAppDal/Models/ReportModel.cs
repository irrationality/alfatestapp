﻿

namespace AlfaTestAppDal
{
    /// <summary>
    /// Модель данных для отчета
    /// </summary>
    public class ReportModel
    { 
        public string name { get; set; }
        public int bookCount { get; set; }
        public double share { get; set; }
    }
}
